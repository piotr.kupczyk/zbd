## 3.1. Drzewo zapytania, plan wykonania zapytania i przykłady operacji optymalizacyjnych.
### Drzewo zapytania
Jest to zapytanie SQL przedstawione w postaci drzewa, gdzie wierzchołki są jedną z operacji `Selekcji, Projekcji lub Złączenia`. 
### Plan wykonania zapytania
Plan wykonania zapytania składania się z:
- drzewa zapytań
- metod dostępu
- metod realizacji
### Przykłady operacji optymalizacyjnych
  - `działanie w miejscu`: polega na nie korzystaniu z tabel tymczasowych podczas zapytań. Zamiast tego przetrzymywane są tylko kursory
    przebeigające rekordy w plikach.
  - `przetwarzanie potokowe`: polega na przekazywaniu wyniku jednego operatora bezpośrednio na wejście drugiego (działanie w miejscu) 



Polega na ustaleniu przez SZBD kolejności wykonywania akcji odczytu i zapisu na obiektach bazy danych.

Wyróżniamy dwa rodzaje planów:
- szeregowy: plan, który wymusza wykonywanie transakcji w sposób synchroniczny - jedna po drugiej
- szeregowalny: plan, który   

## 3.2. Protokół ścisłego blokowania dwufazowego (strict 2-PL).

Służy do zapobiegania konfilktów przy współbieżnie wykonywanych transakcjach. Jest to pesymistyczne podejście do zakładania blokad.

Wyróżnimy dwa typy blokad:

- `shared (S)` - używana przy odczycie
- `exclusive (X)` - używana przy zapisie

Możliwe scenariusze równoległych blokad:

| T1/T2        | S           | X  |
| ------------- |:-------------:| -----:|
| S      | OK | - |
| X      | -      | - |

Jedyna sytuacja, w której możliwe jest równoległe przetwarzanie, to gdy założone są dwie blokady S. 
Żaden inny scenariusz nie jest możliwy.

Strict 2-PL składa się z dwóch faz:
1. transakcja zakłada blokadę i wykonuje operacje
2. transakcja wyoknuje COMMIT lub ROLLBACK i zwalnia blokady

Wszystkie blokady zwalniane są jednocześnie w momencie końca transakcji. 

## 3.3. Realizacja aksjomatów ACID przez SZBD.

SZBD stosuje następujące mechanizmy służące do zapewnienia aksjomatów ACID:

- blokady zakładane na obiekty: ograniczające albo wręcz uniemożliwiające działanie innych
  transakcji na zablokowanym obiekcie,
- dziennik: zapisywanie wszystkich zmian w bazie danych do specjalnego dziennika (logu), aby w razie
  potrzeby móc: dla nie zatwierdzonej transakcji wycofać wprowadzone przez nią zmiany, w przypadku awarii odtworzyć aktualny, spójny
  stan bazy danych.
- wielowersyjność: możliwość odczytywania danych zmienianych równocześnie przez inne transakcje w takiej postaci w
  jakiej istniały w pewnych chwilach w przeszłości (np. w chwili rozpoczynania się danego zapytania lub danej
  transakcji).
- kopia zabezpieczająca bazy danych: wykonywana w regularnych odstępach czasu, na przykład raz na dzień; w
  przypadku awarii danych na dysku pozwala przywrócić spójny stan bazy danych. zapasowa instalacja tej samej bazy danych
  utrzymywana w trybie stand-by.

## 3.4. Realizacja poziomów izolacji przez SZBD.

### SERIALIZABLE:

- Transakcja T uzyskuje blokady do odczytu i zapisu obiektów i zwalnia je zgodnie z protokołem Strict-2PL.
- Zakładane są blokady typu S na zbiory wierszy wynikowych instrukcji SELECT (realizowane albo poprzez zablokowanie
  węzła indeksu albo poprzez zablokowanie całej tabeli).

### REPEATABLE READS:

- Transakcja T uzyskuje blokady do odczytu i zapisu obiektów i zwalnia je zgodnie z protokołem Strict-2PL.

### READ COMMITTED

- Transakcja T uzyskuje blokady X aby wykonać zmiany i utrzymuje te blokady do końca swojego działania.
- Do wykonania odczytu transakcja T uzyskuje blokadę S. Po zakończeniu aktualnej instrukcji blokada ta jest zwalniana (
  nie czekając na koniec transakcji).

### READ UNCOMMITTED

- Transakcja nie zakłada żadnych blokad ale też nie ma prawa wprowadzać żadnych zmian do bazy danych.
- Odczyt dotyczy zawsze aktualnego stanu obiektu - nawet jeśli jest on nie zatwierdzony.

## 3.5. Dwu-fazowe zatwierdzanie (2PC).

`2PC` rozwiązuje problem zatwierdzania (commit) lub odrzucania (rollback) takich samych transakcji wykonywanych równolegle na różnych nodach.

Zasada działania algorytmu:
1. Klient rozpoczyna transakcje
2. Klient wykonuje commit
3. Wiadomość trafia do kordynatora, który wysyła wiadomość `prepare` do wszystkich zaangażowanych nodów.
4. Każdy node wysyła odpowiedź `yes/no`
5. W przypadku pełnej zgodności na każdym nodzie wykonywany jest `commit`.
6. W przeciwnym wypadku wykonywany jest `abort`.

