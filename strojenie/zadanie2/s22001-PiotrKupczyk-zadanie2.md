
## 2.
#### Bez indeksu
![](screenshots/zad2_before.png)
#### Z indeksem
![](screenshots/zad2_after.png)

Odp: Koszt zmniejszył się z 103 do 3. Serwer zamiast Full Scan użył Index Scan.

## 3.
#### Bez indeksu
![](screenshots/zad3_before.png)
#### Z indeksem
![](screenshots/zad3_after.png)

Odp: Koszt ani strategia nie zmieniły się po założeniu indeksu (103, Full Scan). 

## 4.

### Kolumna wartosc
#### Bez indeksu
![](screenshots/zad2_before.png)
#### Z indeksem
![](screenshots/zad4_highly_selective.png)
### Kolumna wartosc2
#### Bez indeksu
![](screenshots/zad3_before.png)
#### Z indeksem
![](screenshots/zad4_poorly_selective.png)

Odp1: Po założeniu `BITMAP INDEX` na kolumnę `wartosc` koszt operacji względem braku indeksu zmniejszył się ze 103 do 76.

Odp2: Po założeniu `BITMAP INDEX` na kolumnę `wartosc2` koszt operacji względem braku indeksu zmniejszył się ze 103 do 78.

## 5.
#### Bez indeksu
![](screenshots/zad5_before.png)
#### Z indeksem
![](screenshots/zad5_after.png)

Odp: Po założeniu `BITMAP INDEX` na obie kolumny, koszt zapytania zmalał z 103 do 76.

## 6.
#### Bez indeksu
![](screenshots/zad6_before.png)
#### Z indeksem
![](screenshots/zad6_after.png)

Odp: Po założeniu indeksu funkcyjnego koszt zapytania zmalał z 103 do 1.


## 7.
#### Bez indeksu
![](screenshots/zad7_before.png)
#### Z indeksem
![](screenshots/zad7_after.png)

Odp: Po stworzeniu tabeli z domyślnym indeksem, koszt zapytania zmalał z 103 do 1.

## 8.
#### Z clustrem
![](screenshots/zad8_with_cluster.png)
#### Bez clustra
![](screenshots/zad8_without_cluster.png)

Odp: Z clustrem indeksowym koszt zapytania wynosił 112 oraz serwer skorzystał z indeksu. Natomiast bez clustra koszt zapytania wynosił 140, a serwer wykonał 2x Full Scan.  

## 9.
#### Z widokiem
![](screenshots/zad9_with_view.png)
#### Bez widoku
![](screenshots/zad9_without_view.png)
#### Bez partycji
![](screenshots/zad9_without_partition.png)
#### Z partycją
![](screenshots/zad9_with_partition.png)

Odp: Korzystając z widoku, koszt zapytania jest znacznie mniejszy (z widokiem: 3.0, bez widoku 145.0). 
W drugiej części zadania serwer skorzystał z partycji i koszt zapytania zmalał z 137.0 do 36.0.