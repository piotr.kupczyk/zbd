CREATE TABLE test (Id INTEGER, Wartosc INTEGER, Wartosc2 INTEGER);

DECLARE
    licz INTEGER;
    wartosc INTEGER;
    wartosc2 INTEGER;
BEGIN
    FOR licz IN 1..100000
        LOOP
            SELECT ROUND(dbms_random.value(1,100000),0) INTO wartosc FROM dual;
            SELECT ROUND(dbms_random.value(1,10),0) INTO wartosc2 FROM dual;
            INSERT INTO S22001.test (Id, Wartosc, Wartosc2) VALUES (licz, wartosc, wartosc2);
        END LOOP;
END;

-- 2

SELECT * FROM test WHERE wartosc = 12345;
CREATE INDEX index_wartosc ON test (Wartosc);
SELECT * FROM test WHERE wartosc = 12345;

-- 3
SELECT * FROM test WHERE wartosc2 = 5;
CREATE INDEX index_wartosc2 ON test (Wartosc2);
SELECT * FROM test WHERE Wartosc2 = 5;

-- 4
DROP INDEX index_wartosc;
DROP INDEX index_wartosc2;

CREATE BITMAP INDEX index_bitmap_wartosc ON test (Wartosc);
CREATE BITMAP INDEX index_bitmap_wartosc2 ON test (Wartosc2);
SELECT * FROM test WHERE wartosc = 12345;
SELECT * FROM test WHERE Wartosc2 = 5;

-- 5
DROP INDEX index_bitmap_wartosc;
DROP INDEX index_bitmap_wartosc2;
SELECT * FROM test WHERE wartosc = 12345 OR wartosc2 = 5;
CREATE BITMAP INDEX index_bitmap_wartosc ON test (Wartosc);
CREATE BITMAP INDEX index_bitmap_wartosc2 ON test (Wartosc2);
SELECT * FROM test WHERE wartosc = 12345 OR wartosc2 = 5;

-- 6
DROP INDEX index_bitmap_wartosc;
DROP INDEX index_bitmap_wartosc2;
SELECT * FROM test WHERE wartosc2 + id = 1234;
CREATE INDEX index_wartosc_and_id ON test (wartosc2 + id);
SELECT * FROM test WHERE wartosc2 + id = 1234;

-- 7
DROP INDEX index_wartosc_and_id;
SELECT * FROM test WHERE id BETWEEN 10000 AND 20000;
DROP TABLE test;
CREATE TABLE test (Id INTEGER PRIMARY KEY, Wartosc INTEGER, Wartosc2 INTEGER) ORGANIZATION INDEX;
SELECT * FROM test WHERE id BETWEEN 10000 AND 20000;

-- 8

CREATE CLUSTER testowy (IdDzial INTEGER);
CREATE INDEX t ON CLUSTER testowy;
CREATE TABLE dzial (IdDzial INTEGER PRIMARY KEY, Nazwa VARCHAR2(10)) CLUSTER testowy(IdDzial);
CREATE TABLE pracownik (IdPracownik INTEGER PRIMARY KEY,Nazwisko VARCHAR2(20),IdDzial INTEGER REFERENCES dzial) CLUSTER testowy(IdDzial);

INSERT INTO dzial (IdDzial, Nazwa) VALUES (1, 'Dzial 1');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (2, 'Dzial 2');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (3, 'Dzial 3');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (4, 'Dzial 4');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (5, 'Dzial 5');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (6, 'Dzial 6');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (7, 'Dzial 7');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (8, 'Dzial 8');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (9, 'Dzial 9');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (10, 'Dzial 10');

DECLARE
    licz INTEGER;
    wartosc INTEGER;
BEGIN
    FOR licz IN 1..100000
        LOOP
            SELECT ROUND(dbms_random.value(1,10),0) INTO wartosc FROM dual;
            INSERT INTO pracownik (IdPracownik, Nazwisko, iddzial) VALUES (licz, 'Nazwisko ' || licz, wartosc);
        END LOOP;
END;

SELECT * FROM Pracownik INNER JOIN Dzial ON Pracownik.IdDzial = Dzial.IdDzial;

drop cluster TESTOWY including tables cascade constraints;

CREATE TABLE dzial (IdDzial INTEGER PRIMARY KEY, Nazwa VARCHAR2(10));
CREATE TABLE pracownik (IdPracownik INTEGER PRIMARY KEY,Nazwisko VARCHAR2(20),IdDzial INTEGER REFERENCES dzial);


INSERT INTO dzial (IdDzial, Nazwa) VALUES (1, 'Dzial 1');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (2, 'Dzial 2');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (3, 'Dzial 3');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (4, 'Dzial 4');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (5, 'Dzial 5');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (6, 'Dzial 6');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (7, 'Dzial 7');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (8, 'Dzial 8');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (9, 'Dzial 9');
INSERT INTO dzial (IdDzial, Nazwa) VALUES (10, 'Dzial 10');

DECLARE
    licz INTEGER;
    wartosc INTEGER;
BEGIN
    FOR licz IN 1..100000
        LOOP
            SELECT ROUND(dbms_random.value(1,10),0) INTO wartosc FROM dual;
            INSERT INTO pracownik (IdPracownik, Nazwisko, iddzial) VALUES (licz, 'Nazwisko ' || licz, wartosc);
        END LOOP;
END;
SELECT * FROM Pracownik INNER JOIN Dzial ON Pracownik.IdDzial = Dzial.IdDzial;



