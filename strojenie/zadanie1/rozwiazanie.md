### 2. Włącz podgląd planu wykonania zapytania i uruchom zapytanie typu:
```
SELECT * 
FROM test 
WHERE Zawartosc = 12345
```
![](2.png)

Odp: Serwer użył Full Scan.

### 3. Załóż indeks niepogrupowany na kolumnę „Zawartość”. Uruchom zapytanie jeszcze raz. Porównaj plan wykonania zapytania oraz koszt. Serwer powinien użyć założonego indeksu (Index seek), a koszt powinien być znacznie niższe.
![](3.png)

Odp: Serwer użył Index Scan, a koszt zmalał z 0.276244 do 0.00328371. 

### 4. Strategia „tylko indeks”. Wykonaj zapytanie i obejrzyj plan jego wykonania:

```
SELECT TerritoryDescription 
FROM Territories 
ORDER BY TerritoryDescription
```
#### Przed
![](4_before.png)
#### Po
![](4_after.png)

Odp: Po dodaniu indeksu, serwer wykonał Full Index Scan zamiast Sort -> Full Table Scan, natomiast koszt zapytania się nie zmienił.

### 5. Strategia „tylko indeks” z „included columns”. Usuń wszystkie indeksy poza kluczem głównym z tabeli Orders. Wykonaj zapytanie:
```
SELECT CustomerId, ShipCountry FROM Orders 
WHERE ShipCountry = 'Austria'
```
#### Przed
![](5_before.png)
#### Po
![](5_after.png)

Odp: Serwer użył Index Scan (Index Seek), a koszt zapytania zmalał z 0.0182691 do 0.003326. 

### 6 Wyszukiwanie zakresowe. Usuń wszystkie indeksy z tabeli utworzonej w punkcie 1 i wykonaj z podglądem planu zapytanie:
```
SELECT * 
FROM test 
WHERE Zawartosc BETWEEN 10000 and 20000
```

#### Przed
![](6_before.png)
#### Po
![](6_after.png)

`CREATE CLUSTERED INDEX index_TerritoryDescription ON Territories(TerritoryDescription)`

Odp: Serwer użył Index Scan (Clustered Index Seek), a czas wykoniania zmalał z 0.276244 do 0.0317273.

### 7. Sortowanie przy pomocy indeksów. Usuń wszystkie indeksy z tabeli utworzonej w punkcie 1 i wykonaj z podglądem planu zapytanie:
```
SELECT * FROM test ORDER BY Zawartosc
```
#### Przed
![](7_before.png)
#### Po
![](7_after.png)

Odp: Po usunięciu indeksu koszt wykonania zapytania zmalał z 0.297725 do 0.209663.