CREATE TABLE test (Id INT IDENTITY, Zawartosc INT)
    GO

DECLARE @a INT
        SET @a = 1
        WHILE @a < 100000 BEGIN
    INSERT INTO test (Zawartosc) VALUES (CONVERT(INT,RAND() * 100000))
        SET @a = @a + 1
END
GO

SELECT * FROM test WHERE Zawartosc = 12345;


-- 4

SELECT TerritoryDescription FROM Territories ORDER BY TerritoryDescription

CREATE CLUSTERED INDEX index_TerritoryDescription ON Territories(TerritoryDescription)

-- 5

SELECT CustomerId, ShipCountry FROM Orders
WHERE ShipCountry = 'Austria'

CREATE NONCLUSTERED INDEX index_ShipCountry_CustomerId ON Orders(ShipCountry, CustomerID)

-- 6

SELECT *
FROM test
WHERE Zawartosc BETWEEN 10000 and 20000

CREATE NONCLUSTERED INDEX index_Zawartosc ON test(Zawartosc)

CREATE CLUSTERED INDEX index_Zawartosc ON test(Zawartosc)

-- 7

DROP INDEX index_Zawartosc on test

SELECT * FROM test ORDER BY Zawartosc

CREATE CLUSTERED INDEX index_Zawartosc ON test(Zawartosc)

