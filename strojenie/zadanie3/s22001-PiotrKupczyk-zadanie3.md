### 1.
![](screenshots/zad1.png)

Odp: Głębokość indeksu w tabeli test wynosi `2`, a w tabeli `test2` 5. Porównując ich rozmiary to w tabeli `test` rozmiar wynosi 7.6KB, a w tabeli `test2` 148.4KB.   

### 2.
#### Bez indeksu
![](screenshots/zad2_before_indexes.png)
#### Niepogrupowany
![](screenshots/zad2_nonclustered.png)
#### Pogrupowany
![](screenshots/zad2_clustered.png)

Odp: Serwer nie skorzystał z niepogrupowanego indeksu. Po założeniu pogrupowanego indeksu koszt zapytania zmalał z
0.433281 do 0.379948 oraz strategia złączenia zmieniła się z Hash Join na Merge Join.

### 3.

#### Bez indeksu
99,999 rows affected in 2 m 22 s 126 ms

#### Z indeksem niepogrupowanym na `Zawartosc`
99,999 rows affected in 2 m 26 s 193 ms

#### Z indeksem pogrupowanym na `Zawartosc`
99,999 rows affected in 2 m 17 s 598 ms

#### Z indeksem niepogrupowanym na `Zawartosc`, niepogrupowanym na `Id` i niepogrupowanym na `Id, Zawartosc`
99,999 rows affected in 2 m 18 s 600 ms

#### Z indeksem pogrupowanym na `id`, niepogrupowanym na `Zawartosc`, niepogrupowanym na `Id` i niepogrupowanym na `Id, Zawartosc`
99,999 rows affected in 2 m 21 s 131 ms

Odp: Zgodnie z powyższymi wynikami wypływ liczby indeksów na wydajność wstawiania jest znikomy (wyniki są obarczone pewnym, niedużym błędem).

### 4.
#### Liczba stron przy 100 000 elementów i fillfactor = 100
![](screenshots/zad4_pages.png)
#### Liczba stron przy 200 000 elementów i fillfactor = 100
![](screenshots/zad4_2_pages.png)
#### Fragmentacja przy 200 000 elementów i fillfactor = 100
![](screenshots/zad4_2_fragmentation.png)
#### Liczba stron przy 100 000 elementów i fillfactor = 50
![](screenshots/zad4_3_pages.png)
#### Liczba stron przy 200 000 elementów i fillfactor = 50
![](screenshots/zad4_3_2_pages.png)
#### Fragmentacja przy 200 000 elementów i fillfactor = 50
![](screenshots/zad4_3_2_fragmentation.png)

Odp: Przy fillfactor = 50% tracimy więcej miejsca w przypadku 100 i 200 tysięcy elementów co jest oczekiwanym rezultatem, ponieważ serwer zostawia 50% leaf-level page puste, żeby poczas późniejszych zmian zniwelować fragmentacje.  
Natomiast po drugim uruchomieniu skryptu, fragmentacja była tylko o 0.32% mniejsza.   


