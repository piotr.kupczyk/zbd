DROP TABLE IF EXISTS test
DROP TABLE IF EXISTS test2
CREATE TABLE test
(
    Id        INT IDENTITY,
    Zawartosc INT
)
CREATE TABLE test2
(
    Id        INT IDENTITY,
    Zawartosc VARCHAR(500)
)
    GO

DECLARE @a INT
        SET @a = 1
        WHILE @a < 100000 BEGIN
    INSERT INTO test (Zawartosc) VALUES (CONVERT(INT, RAND() * 100000))
        SET @a = @a + 1
END
GO

DECLARE @a INT, @txt VARCHAR(500), @txt2 VARCHAR(10)
        SET @a = 1
        SET @txt2 = '1234567890'
        SET @txt = ''
        WHILE @a < 50 BEGIN
        SET @txt = @txt + @txt2
        SET @a = @a + 1
END
SET @a = 1
WHILE @a < 100000 BEGIN
    INSERT INTO test2 (Zawartosc) VALUES (CONVERT(VARCHAR, (CONVERT(INT, RAND() * 100000))) + @txt)
        SET @a = @a + 1
END
GO

CREATE NONCLUSTERED INDEX index_zawartosc ON test (Zawartosc)
CREATE NONCLUSTERED INDEX index_zawartosc ON test2 (Zawartosc)

SELECT *
FROM test
WHERE Zawartosc = 12345
SELECT *
FROM test2
WHERE Zawartosc = '12345'

SELECT INDEXPROPERTY(
               OBJECT_ID('dbo.test'),
               'index_zawartosc', 'IndexDepth'
           ) AS [test Index Depth],
    INDEXPROPERTY(
    OBJECT_ID('dbo.test2'),
    'index_zawartosc', 'IndexDepth'
    ) AS [test2 Index Depth]

-- 2

DROP TABLE test
DROP TABLE test2
CREATE TABLE test
(
    Id        INT PRIMARY KEY,
    Zawartosc VARCHAR(10)
)
CREATE TABLE test2
(
    Id        INT IDENTITY PRIMARY KEY,
    Zawartosc INT,
    IdTest    INT REFERENCES test
)
    GO

INSERT INTO test (Id, Zawartosc)
VALUES (1, 'aaa')
INSERT INTO test (Id, Zawartosc)
VALUES (2, 'bbb')
INSERT INTO test (Id, Zawartosc)
VALUES (3, 'ccc')
INSERT INTO test (Id, Zawartosc)
VALUES (4, 'ddd')
INSERT INTO test (Id, Zawartosc)
VALUES (5, 'eee')

DECLARE @a INT
        SET @a = 1
        WHILE @a < 100000 BEGIN
    INSERT INTO test2 (Zawartosc, IdTest) VALUES (CONVERT(INT, RAND() * 100000), CONVERT(INT, RAND() * 5) + 1)
        SET @a = @a + 1
END
GO

SELECT *
FROM test INNER JOIN test2 ON test2.IdTest = test.id

CREATE NONCLUSTERED INDEX index_IdTest ON test2 (IdTest)
DROP INDEX index_IdTest on test2

ALTER TABLE test2
    DROP CONSTRAINT PK__test2__3214EC070C4F7870
    go
ALTER TABLE test2
    ADD CONSTRAINT PK__test2__3214EC070C4F7870
        PRIMARY KEY NONCLUSTERED (Id)

CREATE CLUSTERED INDEX index_IdTest ON test2 (IdTest)

-- 3
-- tworzenie i wypełnianie bez indeksów
DROP TABLE IF EXISTS test
DROP TABLE IF EXISTS test2
CREATE TABLE test
(
    Id        INT IDENTITY,
    Zawartosc INT
)
    GO

DECLARE @a INT
        SET @a = 1
        WHILE @a < 100000 BEGIN
    INSERT INTO test (Zawartosc) VALUES (CONVERT(INT, RAND() * 100000))
        SET @a = @a + 1
END

-- tworzenie i wypełnianie z indeksem niepogrupowanym na "Zawartosc"
DROP TABLE IF EXISTS test
CREATE TABLE test
(
    Id        INT IDENTITY,
    Zawartosc INT
)
    GO

CREATE NONCLUSTERED INDEX index_Zawartosc ON test(Zawartosc)

DECLARE @a INT
        SET @a = 1
        WHILE @a < 100000 BEGIN
    INSERT INTO test (Zawartosc) VALUES (CONVERT(INT, RAND() * 100000))
        SET @a = @a + 1
END

-- tworzenie i wypełnianie z indeksem pogrupowanym na "Zawartosc"
DROP TABLE IF EXISTS test
CREATE TABLE test
(
    Id        INT IDENTITY,
    Zawartosc INT
)
    GO

CREATE CLUSTERED INDEX index_Zawartosc ON test(Zawartosc)

DECLARE @a INT
        SET @a = 1
        WHILE @a < 100000 BEGIN
    INSERT INTO test (Zawartosc) VALUES (CONVERT(INT, RAND() * 100000))
        SET @a = @a + 1
END

-- tworzenie i wypełnianie z indeksem niepogrupowanym na "Zawartosc" i pogrupowanym na "Id"
DROP TABLE IF EXISTS test
CREATE TABLE test
(
    Id        INT IDENTITY,
    Zawartosc INT
)
    GO

CREATE CLUSTERED INDEX index_clustered_Id ON test(Id)
CREATE NONCLUSTERED INDEX index_nonclustered_Id ON test(Id)
CREATE NONCLUSTERED INDEX index_nonclustered_Zawartosc ON test(Zawartosc)
CREATE NONCLUSTERED INDEX index_nonclustered_id_Zawartosc ON test(id, Zawartosc)

DECLARE @a INT
        SET @a = 1
        WHILE @a < 100000 BEGIN
    INSERT INTO test (Zawartosc) VALUES (CONVERT(INT, RAND() * 100000))
        SET @a = @a + 1
END

-- tworzenie i wypełnianie z indeksem niepogrupowanym na "Zawartosc" i pogrupowanym na "Id"
DROP TABLE IF EXISTS test
CREATE TABLE test
(
    Id        INT IDENTITY,
    Zawartosc INT
)
    GO

CREATE NONCLUSTERED INDEX index_nonclustered_Id ON test(Id)
CREATE NONCLUSTERED INDEX index_nonclustered_Zawartosc ON test(Zawartosc)
CREATE NONCLUSTERED INDEX index_nonclustered_id_Zawartosc ON test(id, Zawartosc)

DECLARE @a INT
        SET @a = 1
        WHILE @a < 100000 BEGIN
    INSERT INTO test (Zawartosc) VALUES (CONVERT(INT, RAND() * 100000))
        SET @a = @a + 1
END

-- 4

DROP TABLE test
CREATE TABLE test (Id INT IDENTITY, Zawartosc INT)
    GO

DECLARE @a INT
        SET @a = 1
        WHILE @a < 100000 BEGIN
    INSERT INTO test (Zawartosc) VALUES (CONVERT(INT,RAND() * 100000))
        SET @a = @a + 1
END
GO

CREATE NONCLUSTERED INDEX index_zawartosc ON test(Zawartosc)
WITH (FILLFACTOR = 100)

SELECT OBJECT_NAME(IXOS.OBJECT_ID)  Table_Name
     ,IX.name  Index_Name
     ,IX.type_desc Index_Type
     ,SUM(PS.[used_page_count]) IndexPages
     ,IXOS.LEAF_INSERT_COUNT NumOfInserts
     ,IXOS.LEAF_UPDATE_COUNT NumOfupdates
     ,IXOS.LEAF_DELETE_COUNT NumOfDeletes

FROM   SYS.DM_DB_INDEX_OPERATIONAL_STATS (NULL,NULL,NULL,NULL ) IXOS
           INNER JOIN SYS.INDEXES AS IX ON IX.OBJECT_ID = IXOS.OBJECT_ID AND IX.INDEX_ID =    IXOS.INDEX_ID
           INNER JOIN sys.dm_db_partition_stats PS on PS.object_id=IX.object_id
WHERE  OBJECTPROPERTY(IX.[OBJECT_ID],'IsUserTable') = 1 AND IX.name = 'index_zawartosc'
GROUP BY OBJECT_NAME(IXOS.OBJECT_ID), IX.name, IX.type_desc,IXOS.LEAF_INSERT_COUNT, IXOS.LEAF_UPDATE_COUNT,IXOS.LEAF_DELETE_COUNT

SELECT  OBJECT_NAME(IDX.OBJECT_ID) AS Table_Name,
        IDX.name AS Index_Name,
        IDXPS.index_type_desc AS Index_Type,
        IDXPS.avg_fragmentation_in_percent  Fragmentation_Percentage
FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) IDXPS
         INNER JOIN sys.indexes IDX  ON IDX.object_id = IDXPS.object_id
    AND IDX.index_id = IDXPS.index_id
    AND IDX.name = 'index_zawartosc'
ORDER BY Fragmentation_Percentage DESC

-- 4 b

DROP TABLE test
CREATE TABLE test (Id INT IDENTITY, Zawartosc INT)
    GO

CREATE NONCLUSTERED INDEX index_zawartosc ON test(Zawartosc)
WITH (FILLFACTOR = 50)

DECLARE @a INT
        SET @a = 1
        WHILE @a < 100000 BEGIN
    INSERT INTO test (Zawartosc) VALUES (CONVERT(INT,RAND() * 100000))
        SET @a = @a + 1
END
GO

SELECT OBJECT_NAME(IXOS.OBJECT_ID)  Table_Name
     ,IX.name  Index_Name
     ,IX.type_desc Index_Type
     ,SUM(PS.[used_page_count]) IndexPages
     ,IXOS.LEAF_INSERT_COUNT NumOfInserts
     ,IXOS.LEAF_UPDATE_COUNT NumOfupdates
     ,IXOS.LEAF_DELETE_COUNT NumOfDeletes

FROM   SYS.DM_DB_INDEX_OPERATIONAL_STATS (NULL,NULL,NULL,NULL ) IXOS
           INNER JOIN SYS.INDEXES AS IX ON IX.OBJECT_ID = IXOS.OBJECT_ID AND IX.INDEX_ID =    IXOS.INDEX_ID
           INNER JOIN sys.dm_db_partition_stats PS on PS.object_id=IX.object_id
WHERE  OBJECTPROPERTY(IX.[OBJECT_ID],'IsUserTable') = 1 AND IX.name = 'index_zawartosc'
GROUP BY OBJECT_NAME(IXOS.OBJECT_ID), IX.name, IX.type_desc,IXOS.LEAF_INSERT_COUNT, IXOS.LEAF_UPDATE_COUNT,IXOS.LEAF_DELETE_COUNT

SELECT  OBJECT_NAME(IDX.OBJECT_ID) AS Table_Name,
        IDX.name AS Index_Name,
        IDXPS.index_type_desc AS Index_Type,
        IDXPS.avg_fragmentation_in_percent  Fragmentation_Percentage
FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) IDXPS
         INNER JOIN sys.indexes IDX  ON IDX.object_id = IDXPS.object_id
    AND IDX.index_id = IDXPS.index_id
    AND IDX.name = 'index_zawartosc'
ORDER BY Fragmentation_Percentage DESC