## 2.
![](screenshots/zadanie2.png)

Odp: Zapytanie z okna pierwszego wykonało się po wykonaniu commit w drugim oknie.
## 3.
![](screenshots/zadanie3.png)

Odp: Zapytanie z okna pierwszego wykonało się bez konieczności wykonania commit w drugim oknie.

## 4.
![](screenshots/zadanie4a.png)

![](screenshots/zadanie4b.png)

Odp: Operacja insert była zablokowana do momentu wykonania commit w drugim oknie.

## 5.
![](screenshots/zadanie5.png)

Odp: Operacja została odrzucona z powyższym błędem.

## 6.

### Sam insert

![](screenshots/zadanie6a.png)

### Sam select

![](screenshots/zadanie6b.png)

### Oba programy jednocześnie
### READ COMMMITTED
#### INSERT
![](screenshots/zadanie6c_insert.png)
#### SELECT
![](screenshots/zadanie6c_select.png)

Odp: Insert - 4128, Select 809

### READ UNCOMMMITTED
#### INSERT
![](screenshots/zadanie6d_insert.png)
#### SELECT
![](screenshots/zadanie6d_select.png)

Odp: Insert - 2999, Select 2949

### SERIALIZABLE
#### INSERT
![](screenshots/zadanie6e_insert.png)
#### SELECT
![](screenshots/zadanie6e_select.png)

Odp: Insert - 0, Select 3265

### SNAPSHOT
#### INSERT
![](screenshots/zadanie6f_insert.png)
#### SELECT
![](screenshots/zadanie6f_select.png)

Odp: Insert - 2068, Select 1999


