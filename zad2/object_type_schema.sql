CREATE TYPE continent_o AS OBJECT
(
    name VARCHAR2(255)
);

CREATE TYPE country_statistics_o AS OBJECT
(
    "year"         NUMERIC,
    population     NUMERIC,
    gdp_in_percent NUMERIC(2, 3)
);

CREATE TYPE language_o AS OBJECT
(
    language VARCHAR2(255)
);

CREATE TYPE statistic_history_o AS VARRAY (300) OF country_statistics_o;

CREATE TYPE languages_o AS VARRAY (15) OF language_o;

CREATE TYPE country_o as OBJECT
(
    name               VARCHAR2(255),
    area               VARCHAR2(255),
    continent          continent_o,
    languages          languages_o,
    statistics_history statistic_history_o
);

CREATE TABLE continent OF continent_o;
CREATE TABLE language OF language_o;
CREATE TABLE country_statistics OF country_statistics_o;
CREATE TABLE country OF country_o;