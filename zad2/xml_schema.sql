CREATE TABLE product
(
    id                NUMERIC PRIMARY KEY,
    name              VARCHAR2(255),
    vendor            VARCHAR2(255),
    short_description VARCHAR2(255),
    stock             number,
    price             number(10, 2)
);

CREATE TABLE customer
(
    id      NUMERIC,
    name    varchar2(255),
    surname varchar2(255),
    phone   varchar2(12),
    address XMLTYPE
);

CREATE TABLE "order"
(
    id           NUMERIC PRIMARY KEY,
    order_date   date,
    shipped_date date,
    status       varchar2(255),
    comments     XMLTYPE,
    details      XMLTYPE
);