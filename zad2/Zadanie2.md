# Proszę podać dwa interesujące schematy baz danych (inne od tych w wykładach): jeden na bazę obiektową, drugi na bazę XML - z użyciem typu danych XMLType (wykłady 3 i 4).

## Baza obiektowa

Schema bazy danych dotyczy przechowywania danych o państwach.

Każde państwo składa się z nazwy, rozmiaru w km^2, kontytnetu na którym leży, listy języków urzędowych oraz statystyk 
z poszczególnych lat.
Statystyki dotyczą roku, w którym zostały zebrane, populacji na dany rok oraz PKB.   

```
CREATE TYPE continent_o AS OBJECT
(
    name VARCHAR2(255)
);

CREATE TYPE country_statistics_o AS OBJECT
(
    "year"         NUMERIC,
    population     NUMERIC,
    gdp_in_percent NUMERIC(2, 3)
);

CREATE TYPE language_o AS OBJECT
(
    language VARCHAR2(255)
);

CREATE TYPE statistic_history_o AS VARRAY (300) OF country_statistics_o;

CREATE TYPE languages_o AS VARRAY (15) OF language_o;

CREATE TYPE country_o as OBJECT
(
    name               VARCHAR2(255),
    area               VARCHAR2(255),
    continent          continent_o,
    languages          languages_o,
    statistics_history statistic_history_o
);

CREATE TABLE continent OF continent_o;
CREATE TABLE language OF language_o;
CREATE TABLE country_statistics OF country_statistics_o;
CREATE TABLE country OF country_o;
```

## Baza XML

Schema bazy danych dotyczy przechowywania danych o produktach, zamówieniach i klientach.

Produkt składa sięz nazwy, producenta, krótkiego opisu, ilości dostępnych sztuk w magazynie oraz ceny.

W tabeli klientów przechowywane są dane o imieniu, nazwisku, numerze telefonu oraz adresie. 

W tabela zamówienie są dane o dacie złożenia i wysyłki, obecnym statusie i liście komentarzy. 
Pole detale informuje o konkretnym zamówieniu klienta - produktach i ich ilości.   

```
CREATE TABLE product
(
    id                NUMERIC PRIMARY KEY,
    name              VARCHAR2(255),
    vendor            VARCHAR2(255),
    short_description VARCHAR2(255),
    stock             number,
    price             number(10, 2)
);

CREATE TABLE customer
(
    id      NUMERIC,
    name    varchar2(255),
    surname varchar2(255),
    phone   varchar2(12),
    address XMLTYPE
);

CREATE TABLE "order"
(
    id           NUMERIC PRIMARY KEY,
    order_date   date,
    shipped_date date,
    status       varchar2(255),
    comments     XMLTYPE,
    details      XMLTYPE
);
```
